<?Php

require "ONLINEDBAPI.php";
require('../payments/pdf/fpdf.php');

class myPDF extends FPDF{
    function  header(){
     $this->image('img/download.jpg',10,10,-200);   
     $this->SetFont('Arial','B',14);
     $this->Cell(276,10,' Payments Report by Account',0,0,'C');
     $this->Ln();
     $this->SetFont('Times','',12);
     $this->Cell(276,10,'Rate Payer Details',0,0,'C');
     $this->Ln(20);
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,10,'Page'.$this->PageNo().'/{nb}',0,0,'C');


    }
    function headerTable(){
        $this->SetFont('Times','B',12);
        $this->Cell(40,10,'Account',1,0,'C');
        $this->Cell(60,10,'Amount Paid',1,0,'C');
        $this->Cell(60,10,'Payment Reference',1,0,'C');
        $this->Cell(60,10,'Income Code',1,0,'C');
        $this->Cell(50,10,'Date of Payment',1,0,'C');
        $this->Ln();


    }
    function viewTable(){
        $this->SetFont('Times','',10);
        $saved = getDates();
        $start = @$saved[0]['startDate'];
        $end = @$saved[0]['endDate'];
        $code = @$saved[0]['code'];
        $account = @$saved[0]['account'];
        
        
        
       $data =  AccountReport($start,$end,$code,$account);
       foreach($data as $da){
           $acc= @$da['CustomerNumber'];
           $amount= @$da['Balance'];
           $reference= @$da['reference'];
           $date= @$da['LastPayDate'];
           $type= @$da['balancetype'];
           $desc = description($type);
           $product = @$desc[0]['description'];
    $this->Cell(40,10,$acc,1,0,'C');
    $this->Cell(60,10,$amount,1,0,'C');
    $this->Cell(60,10,$reference,1,0,'C');
    $this->Cell(60,10,$product,1,0,'C');
    $this->Cell(50,10,$date,1,0,'C');
    $this->Ln();
       }







    }




}

$pdf = new myPDF(); 
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable();
$pdf->SetFont('Arial','B',12);
$pdf->Output('PaymentsByAccount.pdf','I');
?>