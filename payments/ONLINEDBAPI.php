<?php
require "STARTDBAPI.php";
$server = $_SESSION['server'];
$db = $_SESSION['dbname'];
$dbuser = $_SESSION['username'];
$dbpass = $_SESSION['dbpassword'];
//$dbn = new PDO("sqlsrv:Server=sql5031.site4now.net;Database=DB_A33C8A_promunAPI", "DB_A33C8A_promunAPI_admin", "promun14arundel");
try{
    $dbn = new PDO("sqlsrv:Server=$server;Database=$db", "$dbuser", "$dbpass");
    $dbn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (Exception $ex){
    echo "<script>alert('Could not database ($db)user($dbuser)password($dbpass) for Local Authority');</script>";
    die();
}
function UserLogin($passCode,$passWord) {
    //load users before trying to login
    global $dbn;
    //LaunchUsers($urlUsers);
    try {
        $sql = $dbn->prepare("select * from users where username = ? and passWord = ?");
        $sql->execute(array($passCode,$passWord));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $_SESSION['user'] = @$result[0]['username'];
            $status['status'] = 'passed';
            header("location:profile.php");
        } else {
            $status['status'] = 'Failed';
         echo "<script>alert('Error: Invalid Login Credentials '); location='login.php'</script>";

           
        }
    } catch (Exception $ex) {
        $status['status'] = $ex->getMessage();
    }

    return $status;
}
function checkCo($code){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from company where code=?");
        $sql->execute(array($code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
            
        } else {
            $status['status'] = 'Failed';
           
        }
       
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $status;
}

function regUser($user,$password,$code){
    global $dbn;
    try {
        
            $sql = $dbn->prepare('insert into users("username", "password", "code") values(?,?,?) ');
            $sql->execute(array($user,$password,$code));
            $count = $sql->rowCount();
            if ($count > 0) {
                $result['status'] = 'ok';
            } else {
                $result['status'] = 'fail';
            }
    
        }catch (Exception $ex) {
            $result['status'] = $ex->getMessage();
        }
        return $result;

}



function checkName($code) {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from company  where code=?");
        $sql->execute(array($code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
/*function connect($LA){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from LA where name=?");
        $sql->execute(array($LA));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $status['status'] = 'passed';
            
        } else {
            $status['status'] = 'Failed';
           
        }
       
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }

    return $result;
}*/

function regCO($name,$code){
    global $dbn;
    try {
        
            $sql = $dbn->prepare('insert into company("name", "code") values(?,?) ');
            $sql->execute(array($name,$code));
            $count = $sql->rowCount();
            if ($count > 0) {
                $result['status'] = 'ok';
            } else {
                $result['status'] = 'fail';
            }
    
        }catch (Exception $ex) {
            $result['status'] = $ex->getMessage();
        }
        return $result;

}
function deleteCode() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from codeView");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function DeleteDateView() {
    global $dbn;
    try {

        $stmt = $dbn->prepare("delete from dateView");
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function saveCode($code){
    global $dbn;
    try {
        
            $sql = $dbn->prepare('insert into codeView("code") values(?) ');
            $sql->execute(array($code));
            $result = $sql->fetchALL(PDO::FETCH_ASSOC);
            /*$count = $sql->rowCount();
            if ($count > 0) {
                $result['status'] = 'ok';
            } else {
                $result['status'] = 'fail';
            }*/
    
        }catch (Exception $ex) {
            //$result['status'] = $ex->getMessage();
            $result = $ex->getMessage();
        }
        return $result;

}
function insertDates($startDate,$endDate,$code){
    global $dbn;
    try {
        
            $sql = $dbn->prepare('insert into dateView("startDate","endDate","code") values(?,?,?) ');
            $sql->execute(array($startDate,$endDate,$code));
            $count = $sql->rowCount();
            if ($count > 0) {
                $result['status'] = 'ok';
            } else {
                $result['status'] = 'fail';
            }
    
        }catch (Exception $ex) {
            $result['status'] = $ex->getMessage();
        }
        return $result;

}
function insertDate($start,$end){
    global $dbn;
    try {
        
            $sql = $dbn->prepare('insert into dateView("startDate","endDate") values(?,?) ');
            $sql->execute(array($start,$end));
            $count = $sql->rowCount();
            if ($count > 0) {
                $result['status'] = 'ok';
            } else {
                $result['status'] = 'fail';
            }
    
        }catch (Exception $ex) {
            $result['status'] = $ex->getMessage();
        }
        return $result;

}
function insertDetails($startDate,$endDate,$code,$account){
    global $dbn;
    try {
        
            $sql = $dbn->prepare('insert into dateView("startDate","endDate","code","account") values(?,?,?,?) ');
            $sql->execute(array($startDate,$endDate,$code,$account));
            $count = $sql->rowCount();
            if ($count > 0) {
                $result['status'] = 'ok';
            } else {
                $result['status'] = 'fail';
            }
    
        }catch (Exception $ex) {
            $result['status'] = $ex->getMessage();
        }
        return $result;

}

function Payments($start,$end,$code){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from tblPaynowPayments where LastPayDate >=? and LastPayDate <=? and code=? ORDER BY LastPayDate ASC");
        $sql->execute(array($start,$end,$code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function Income($start,$end){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from tblPaynowPayments where LastPayDate >=? and LastPayDate <=?  ORDER BY LastPayDate ASC");
        $sql->execute(array($start,$end));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function company(){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from company");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function AccountReport($start,$end,$code,$account){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from tblPaynowPayments where LastPayDate >=? and LastPayDate <=? and code=? and CustomerNumber=?");
        $sql->execute(array($start,$end,$code,$account));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function  getDates(){
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from dateView");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        //print_r($result);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getNewCode() {
    global $dbn;
    try {
        $sql = $dbn->prepare("select * from codeView");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
function getCode($passCode){
 global $dbn;
try {
    $sql = $dbn->prepare("select * from users where username=?");
    $sql->execute(array($passCode));
    $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    //print_r($result);
} catch (Exception $ex) {
    $result = $ex->getMessage();
}

return $result;
}

function redirect($url) {
    header("Location: $url");
}

function syncedPayments($code){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from tblPaynowPayments where flag ='True' and code = ?");
        $sql->execute(array($code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function unsynced($code){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from tblPaynowPayments where flag ='False' and code = ?");
        $sql->execute(array($code));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function description($type){
    global $dbn;
    try {

        $sql = $dbn->prepare("select * from lubalanceType where balanceType =?");
        $sql->execute(array($type));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function logout() {
    session_destroy();
    unset($_SESSION['user']);
    unset($_SESSION['server']);
    unset($_SESSION['dbname']);
    unset($_SESSION['username']);
    unset($_SESSION['dbpassword']);
    return true;
}





